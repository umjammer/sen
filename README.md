[![Latest Release](https://gitlab.com/umjammer/sen/-/badges/release.svg)](https://gitlab.com/umjammer/sen/-/releases)
[![pipeline status](https://gitlab.com/umjammer/sen/badges/master/pipeline.svg)](https://gitlab.com/umjammer/sen/-/commits/master)
![Java](https://img.shields.io/badge/Java-8-b07219)

# Morphological analysis system Sen

 * Sen is a Japanese morphological analysis system written in Java.
 * A port of [MeCab](http://taku910.github.io/mecab/) developed in C ++ to Java.
 * Dictionary MeCab, [Chasen](https://chasen-legacy.osdn.jp/) use the dictionary of the same IPA with.

## Install

  * maven ... add below into `pom.xml`

```xml
    <repository>
      <id>gitlab-maven</id>
      <url>https://gitlab.com/api/v4/projects/18523814/packages/maven</url>
    </repository>
```

```xml
    <dependency>
      <groupId>sen</groupId>
      <artifactId>sen</artifactId>
      <version>1.2.3</version>
    </dependency>
```

  * dictionary

```
$ git clone https://gitlab.com/umjammer/sen.git
$ mvn install
```

you need to add an environment variable `SEN_HOME=/your_dir/src/main/home`

### Warning

don't modify `src/main/home/dic/*.pl` encodings!

## Sample

 * [vav-speech](https://github.com/umjammer/vavi-speech/blob/master/src/main/java/vavi/speech/phonemizer/SenJaPhonemizer.java)

## References

 * https://qiita.com/hoto17296/items/97c7ec64923625addf01

## TODO

 * when making dictionary check update
 * enable lucene-gosen test cases
 * pass logging jar classpath to the ant task