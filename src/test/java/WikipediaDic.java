import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.GZIPInputStream;


public class WikipediaDic {

    private static final String inputFileName = "jawiki-latest-all-titles-in-ns0.gz";
    private static final String outputFileName = "Noun.wikipedia.dic";
    private static final String inputEncoding = "UTF-8";
    private static final String outputEncoding = "EUC-JP";

    public void run() throws Exception {
        URL url = new URL("http://download.wikimedia.org/jawiki/latest/" + inputFileName);
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new BufferedInputStream(uc.getInputStream())),
                inputEncoding));
        PrintStream printStream = new PrintStream(new BufferedOutputStream(Files.newOutputStream(Paths.get(outputFileName))),
                false,
                outputEncoding);

        String line;
        while ((line = reader.readLine()) != null) {
            if (line.isEmpty() || line.contains("_")) {
                continue;
            }

            System.err.printf("(品詞 (名詞 固有名詞 一般)) ((見出し語 (%s 10000)))\n", line);
            printStream.printf("(品詞 (名詞 固有名詞 一般)) ((見出し語 (%s 10000)))\n", line);
        }
        printStream.close();
        reader.close();
    }

    public static void main(String[] args) throws Exception {
        new WikipediaDic().run();
    }
}
