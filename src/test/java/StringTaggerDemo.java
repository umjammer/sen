/*
 * StringTaggerDemo.java - StringTaggerDemo is demonstration program for Sen.
 *
 * Copyright (C) 2002-2004 Takashi Okamoto Takashi Okamoto <tora@debian.org>
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import net.java.sen.StringTagger;
import net.java.sen.Token;
import org.junit.jupiter.api.Test;
import vavix.util.Checksum;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StringTaggerDemo {

    /**
     * @param args
     * <pre>
     *     surface (basicString) pos(start,end.length) reading pronunciation
     *     今日	(今日)	名詞-副詞可能(53,55,2)	キョウ	キョー
     * </pre>
     */
    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("done.\nPlease input Japanese sentence:");

            StringTagger tagger = StringTagger.getInstance();
            // You can also get StringTagger instance by following code:
//            String confPath = System.getProperty("sen.home")
//                    + System.getProperty("file.separator") + "conf/sen.xml";
//            tagger = StringTagger.getInstance(confPath);

            String s;
            while ((s = br.readLine()) != null) {
                Token[] token = tagger.analyze(s);
                if (token != null) {
                    for (Token value : token) {
                        System.out.println(value.toString() + "\t("
                                + value.getBasicString() + ")" + "\t" + value.getPos()
                                + "(" + value.start() + "," + value.end() + ","
                                + value.length() + ")\t" + value.getReading() + "\t"
                                + value.getPronunciation()

                        );
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void test1() throws Exception {
        String text = "本来は、貧困層の女性や子供に医療保護を提供するために創設された制度である、アメリカ低所得者医療援助制度が、今日では、その予算の約３分の１を老人に費やしている。";
        StringTagger tagger = StringTagger.getInstance();
        Token[] tokens = tagger.analyze(text);
        Path out = Paths.get("tmp", "out.txt");
        if (!Files.exists(out.getParent())) {
            Files.createDirectories(out.getParent());
        }
        PrintWriter pr = new PrintWriter(new OutputStreamWriter(Files.newOutputStream(out)));
        for (Token token : tokens) {
            pr.println(token.toString() + "\t("
                    + token.getBasicString() + ")" + "\t" + token.getPos()
                    + "(" + token.start() + "," + token.end() + ","
                    + token.length() + ")\t" + token.getReading() + "\t"
                    + token.getPronunciation()
            );
        }
        pr.flush();
        pr.close();
        assertEquals(Checksum.getChecksum(Paths.get("src/test/resources/morph.txt")), Checksum.getChecksum(out));
    }
}
