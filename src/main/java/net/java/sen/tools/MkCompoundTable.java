/*
 * MkCompoundTable.java - MkCompoundTable utility to make compound word table.
 *
 * Copyright (C) 2004 Tsuyoshi Fukui
 * Tsuyoshi Fukui <fukui556@oki.com>
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package net.java.sen.tools;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import net.java.sen.util.CSVParser;


public class MkCompoundTable {
    private static Logger log = Logger.getLogger(MkCompoundTable.class.getName());

    /**
     * Build compound word table.
     */
    public static void main(String[] args) {
        ResourceBundle rb = ResourceBundle.getBundle("dictionary");
        int pos_start = Integer.parseInt(rb.getString("pos_start"));
        int pos_size = Integer.parseInt(rb.getString("pos_size"));

        try {
            log.fine("reading compound word information ... ");
            Map<String, String> compoundTable = new HashMap<>();

            log.fine("load dic: " + rb.getString("compound_word_file"));
            BufferedReader dicStream = new BufferedReader(
                    new InputStreamReader(Files.newInputStream(Paths.get(
                            rb.getString("compound_word_file"))),
                            rb.getString("dic.charset")));

            String t;
            int line = 0;

            StringBuilder pos_b = new StringBuilder();
            while ((t = dicStream.readLine()) != null) {
                CSVParser parser = new CSVParser(t);
                String[] csv = parser.nextTokens();
                if (csv.length < (pos_size + pos_start)) {
                    throw new IllegalStateException("format error:" + line);
                }

                pos_b.setLength(0);
                for (int i = pos_start; i < (pos_start + pos_size - 1); i++) {
                    pos_b.append(csv[i]);
                    pos_b.append(',');
                }

                pos_b.append(csv[pos_start + pos_size - 1]);
                pos_b.append(',');

                for (int i = pos_start + pos_size; i < (csv.length - 2); i++) {
                    pos_b.append(csv[i]);
                    pos_b.append(',');
                }
                pos_b.append(csv[csv.length - 2]);
                compoundTable.put(pos_b.toString(), csv[csv.length - 1]);
            }
            dicStream.close();
            log.fine("done.");
            log.fine("writing compound word table ... ");
            ObjectOutputStream os = new ObjectOutputStream(
                    Files.newOutputStream(Paths.get(rb.getString("compound_word_table"))));
            os.writeObject(compoundTable);
            os.close();
            log.fine("done.");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}