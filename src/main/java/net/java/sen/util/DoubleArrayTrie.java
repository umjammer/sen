// even is base array for Double Array Trie

// odd is check array for Double Array Trie


package net.java.sen.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class DoubleArrayTrie {

    private final static int BUF_SIZE = 500000;
    private static Logger log = Logger.getLogger(DoubleArrayTrie.class.getName());

    // base and check arrays for double array trie
    private int[] array;
    private int[] used;
    private int size;
    // buffe size for check and base arrays.
    private int alloc_size;
    private char[][] str;
    private int str_size;
    private int[] len;
    private int[] val;

    private int next_check_pos;
    private int no_delete;

    private static class Node {
        int code;
        int depth;
        int left;
        int right;
    }

    public DoubleArrayTrie() {
        array = null;
        used = null;
        size = 0;
        alloc_size = 0;
        no_delete = 0;
    }

    public void load(String fileName) throws IOException {
        log.fine("loading double array trie dict = " + fileName);
        long start = System.currentTimeMillis();
        File file = new File(fileName);
        array = new int[(int) (file.length() / 4)];
        DataInputStream is = new DataInputStream
                (new BufferedInputStream(Files.newInputStream(file.toPath()), BUF_SIZE));
        for (int i = 0; i < array.length; i++) {
            array[i] = is.readInt();
        }
        log.fine("loaded time = " + (((double) (System.currentTimeMillis() - start)) / 1000) + "[ms]");
    }

    int[] _resize(int[] ptr, int n, int l, int v) {
        int[] tmp = new int[l]; // realloc
        if (ptr != null) {
            l = ptr.length;
        } else {
            l = 0;
        }

        for (int i = 0; i < l; i++) tmp[i] = ptr[i]; // copy
        for (int i = l; i < l; i++) tmp[i] = v;
        ptr = null;
        return tmp;
    }

    int resize(int new_size) {
        array = _resize(array, alloc_size << 1, new_size << 1, 0);
        used = _resize(used, alloc_size, new_size, 0);

        alloc_size = new_size;

        return new_size;
    }

    int fetch(Node parent, List<Node> siblings) {
        int prev = 0;
        log.finest("parent.left=" + parent.left);
        log.finest("parent.right=" + parent.right);
        log.finest("parent.depth=" + parent.depth);

        for (int i = parent.left; i < parent.right; i++) {
            if (((len != null) ? len[i] : str[i].length) < parent.depth)
                continue;

            char[] tmp = str[i];

            int cur = 0;
            if (((len != null) ? len[i] : str[i].length) != parent.depth) {
                log.finest("tmp[" + parent.depth + "]=" + tmp[parent.depth]);
                cur = (int) tmp[parent.depth] + 1;
            }

            if (prev > cur) {
                log.severe("given strings are not sorted.\n");
                throw new IllegalStateException("Fatal: given strings are not sorted.\n");
            }

            if (cur != prev || siblings.size() == 0) {
                Node tmp_node = new Node();
                tmp_node.depth = parent.depth + 1;
                tmp_node.code = cur;
                tmp_node.left = i;
                if (siblings.size() != 0)
                    siblings.get(siblings.size() - 1).right = i;

                siblings.add(tmp_node);
            }

            prev = cur;
        }

        if (siblings.size() != 0)
            siblings.get(siblings.size() - 1).right = parent.right;

        return siblings.size();
    }

    int insert(List<Node> siblings) {
        int begin = 0;
        int pos = (Math.max((siblings.get(0).code + 1), next_check_pos)) - 1;
        int nonzero_num = 0;
        int first = 0;

        while (true) {
            pos++;
            {
                int t = pos;
                if (t > alloc_size) {
                    resize((int) (t * 1.05));
                }
            }

            if (array[(pos << 1) + 1] != 0) {
                nonzero_num++;
                continue;
            } else if (first == 0) {
                next_check_pos = pos;
                first = 1;
            }

            begin = pos - siblings.get(0).code;

            {
                int t = begin + siblings.get(siblings.size() - 1).code;
                if (t > alloc_size) {
                    resize((int) (t * 1.05));
                }
            }

            if (used[begin] != 0) continue;

            boolean flag = false;

            for (int i = 1; i < siblings.size(); i++) {
                if (array[((begin + siblings.get(i).code) << 1) + 1] != 0) {
                    flag = true;
                    break;
                }
            }
            if (!flag) break;
        }

        // -- Simple heuristics --
        // if the percentage of non-empty contents in check between the index
        // 'next_check_pos' and 'check' is grater than some constant value (e.g. 0.9),
        // new 'next_check_pos' index is written by 'check'.
        if (1.0 * nonzero_num / (pos - next_check_pos + 1) >= 0.95) next_check_pos = pos;
        used[begin] = 1;
        size = (Math.max((size), (begin + siblings.get(siblings.size() - 1).code + 1)));
        for (Node o : siblings) {
            array[((begin + o.code) << 1) + 1] = begin;
        }

        for (Node sibling : siblings) {
            List<Node> new_siblings = new ArrayList<>();

            if (fetch(sibling, new_siblings) == 0) {
                array[(begin + sibling.code) << 1] =
                        (val != null) ?
                                (-val[sibling.left] - 1)
                                : (-sibling.left - 1);

                if ((val != null) && ((-val[sibling.left] - 1) >= 0)) {
                    log.severe("negative value is assgined.");
                    throw new IllegalStateException("Fatal: negative value is assgined.");
                }

            } else {
                int ins = insert(new_siblings);
                array[(begin + sibling.code) << 1] = ins;
            }
        }

        return begin;
    }

    void clear() {
        array = null;
        used = null;
        alloc_size = 0;
        size = 0;
        no_delete = 0;
    }

    int get_unit_size() {
        return 8;
    }

    int get_size() {
        return size;
    }

    int get_nonzero_size() {
        int result = 0;
        for (int i = 0; i < size; i++)
            if (array[(i << 1) + 1] != 0) result++;
        return result;
    }

    public int build(char[][] _str,
                     int[] _len,
                     int[] _val) {
        return build(_str, _len, _val, _str.length);
    }

    public int build(char[][] _str,
                     int[] _len,
                     int[] _val,
                     int size) {
        if (_str == null) return 0;
        if (_str.length != _val.length) {
            log.warning("index and text should be same size.");
            return 0;
        }

        str = _str;
        len = _len;
        str_size = size;
        val = _val;

        resize(1024 * 10);

        array[0 << 1] = 1;
        next_check_pos = 0;

        Node root_node = new Node();
        root_node.left = 0;
        root_node.right = str_size;
        root_node.depth = 0;

        List<Node> siblings = new ArrayList<>();
        log.finest("---fetch---");
        fetch(root_node, siblings);
        log.finest("---insert---");
        insert(siblings);

        used = null;

        return size;
    }

    public int search(char[] key,
                      int pos,
                      int len) {
        if (len == 0) len = key.length;

        int b = array[0 << 1];
        int p;
        for (int i = pos; i < len; i++) {
            p = b + key[i] + 1;
            if (b == array[(p << 1) + 1]) b = array[p << 1];
            else return -1;
        }

        p = b;
        int n = array[p << 1];

        if (b == array[(p << 1) + 1] && n < 0) return -n - 1;
        return -1;
    }

    public int commonPrefixSearch(char[] key,
                                  int[] result,
                                  int pos,
                                  int len) {
        if (len == 0) len = key.length;

        int b = array[0 << 1];
        int num = 0;
        int n;
        int p;

        for (int i = pos; i < len; i++) {

            // ignore new line and line feed.
            // this may causes error when use latain character.
            //      if(key[i]=='\n'||key[i]=='\r')continue;

            p = b; // + 0;
            n = array[p << 1];
            if (b == array[(p << 1) + 1] && n < 0) {
                log.finest("result[" + num + "]=" + (-n - 1));
                if (num < result.length) {
                    result[num] = -n - 1;
                } else {
                    log.warning("result array size may not enough");
                }
                num++;
            }

            p = b + key[i] + 1;

            // following lines are temporary code to resolve OutOfArrayException.
            // TODO:fixme
            if ((p << 1) > array.length) {
                log.warning("p range is over.");
                log.warning("(p<<1,array.length)=(" + (p << 1) + "," + array.length + ")");
                return num;
            }
            // end of temporary code.

            if (b == array[(p << 1) + 1]) {
                b = array[p << 1];
            } else {
                return num;
            }
        }

        p = b;
        n = array[p << 1];
        if (b == array[(p << 1) + 1] && n < 0) {
            log.finest("result[" + num + "]=" + (-n - 1));
            if (num < result.length) {
                result[num] = -n - 1;
            } else {
                log.warning("result array size may not enough");
            }
            num++;
        }

        return num;
    }

    public void save(String file) throws IOException {
        long start = System.currentTimeMillis();
        DataOutputStream out = new DataOutputStream
                (new BufferedOutputStream(Files.newOutputStream(Paths.get(file))));
        int dsize = alloc_size << 1;
        for (int i = 0; i < dsize; i++) {
            out.writeInt(array[i]);
        }
        out.close();
        log.fine("save time = " +
                (((double) (System.currentTimeMillis() - start)) / 1000) + "[s]");
    }

    // for debug
    public static void dumpChar(char[] c, String message) {
        System.err.println("message=" + message);
        for (char value : c) {
            System.err.print(value + ",");
        }
        System.err.println();
    }

    public static void main(String[] args) {
        //    DoubleArrayTrie da = new DoubleArrayTrie();
    }
}



