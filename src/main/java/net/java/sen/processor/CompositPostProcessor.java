/*
 * CompositPostProcessor.java - Composit postprocessor for tagger.
 *
 * Copyright (C) 2004 Tsuyoshi Fukui
 * Tsuyoshi Fukui <fukui556@oki.com>
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package net.java.sen.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import net.java.sen.Token;


public class CompositPostProcessor implements PostProcessor {

    private List<Rule> rules = new ArrayList<>();

    public void readRules(BufferedReader reader) throws IOException {
        String line = null;
        while ((line = reader.readLine()) != null) {
            StringTokenizer st = new StringTokenizer(line);
            if (!st.hasMoreTokens()) {
                continue;
            }

            Set<String> ruleSet = new HashSet<>();
            String first = st.nextToken();
            if (!st.hasMoreTokens()) {
                // 1個しか無い場合は、連結品詞名であり、構成品詞名でもある
                removeFromOtherRules(first);
                ruleSet.add(first);
                rules.add(new Rule(first, ruleSet));
                continue;
            }

            while (st.hasMoreTokens()) {
                String pos = st.nextToken();
                removeFromOtherRules(pos);
                ruleSet.add(pos);
            }
            rules.add(new Rule(first, ruleSet));
        }
    }

    private void removeFromOtherRules(String pos) {
        for (Rule rule : rules) {
            if (rule.contains(pos)) {
                rule.remove(pos);
                return;
            }
        }
    }

    public List<Rule> getRules() {
        return rules;
    }

    public Token[] process(Token[] tokens, Map<String, List<Token>> postProcessInfo) {
        if (tokens.length == 0) {
            return tokens;
        }

        List<Token> newTokens = new ArrayList<>();
        Token prevToken = null;
        Rule currentRule = null;
        outer_loop:
        for (int i = 0; i < tokens.length; i++) {
            if (currentRule != null) {
                if (prevToken.end() != tokens[i].start()
                        || !currentRule.contains(tokens[i].getPos())) {
                    currentRule = null;
                    newTokens.add(prevToken);
                    prevToken = null;
                } else {
                    merge(prevToken, tokens[i], currentRule.getPos());
                    if (i == tokens.length - 1) {
                        newTokens.add(prevToken);
                        prevToken = null;
                    }
                    continue;
                }
            }
            for (Rule rule : rules) {
                if (rule.contains(tokens[i].getPos())) {
                    currentRule = rule;
                    prevToken = tokens[i];
                    continue outer_loop;
                }
            }
            currentRule = null;
            newTokens.add(tokens[i]);
        }
        if (prevToken != null) {
            newTokens.add(prevToken);
        }
        return newTokens.toArray(new Token[0]);
    }

    private void merge(Token prev, Token current, String newPos) {
        if (prev == null) {
            return;
        }

        prev.setBasicString(prev.getBasicString() + current.getBasicString());
        prev.setCost(prev.getCost() + current.getCost());
        prev.setPos(newPos);
        prev.setPronunciation(prev.getPronunciation()
                + current.getPronunciation());
        prev.setReading(prev.getReading() + current.getReading());
        prev.setLength(prev.length() + current.length());
        prev.setSurface(null);
    }

    static class Rule {
        private String pos;
        private Set<String> ruleSet;

        public Rule(String pos, Set<String> ruleSet) {
            this.pos = pos;
            this.ruleSet = ruleSet;
        }

        public String getPos() {
            return pos;
        }

        public boolean contains(String pos) {
            return ruleSet.contains(pos);
        }

        public void remove(String pos) {
            ruleSet.remove(pos);
        }

        public String toString() {
            StringBuilder buf = new StringBuilder(pos);
            for (String o : ruleSet) {
                buf.append(" ").append(o);
            }
            return new String(buf);
        }
    }
}